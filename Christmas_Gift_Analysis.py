#!/usr/bin/env python
# coding: utf-8

# # Christmas Gift Analysis using ML Algorithms
# #By- Aarush Kumar
# #Dated: June 23,2021

# In[1]:


import pandas as pd  
import numpy as np 
import matplotlib.pyplot as plt 
import seaborn as sns
get_ipython().run_line_magic('matplotlib', 'inline')
# Data Transform
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
# Train Model
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
# Metrics
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
import statsmodels.api as sm


# In[2]:


train = pd.read_csv('/home/aarush100616/Downloads/Projects/Christmas Gift Analysis/train.csv')
test = pd.read_csv('/home/aarush100616/Downloads/Projects/Christmas Gift Analysis/test.csv')


# In[3]:


train.shape


# In[4]:


train.size


# In[5]:


test.shape


# In[6]:


test.size


# In[7]:


data = train.copy()


# In[8]:


train.head()


# In[9]:


test.head()


# In[10]:


data.isnull().sum()


# In[11]:


data['volumes'].notna().sum()


# In[12]:


data['volumes'].describe()


# In[13]:


data = data.dropna(axis=1)


# In[14]:


data.isnull().sum()


# In[15]:


data.dtypes


# In[16]:


desc = train.describe().T
f, ax = plt.subplots(figsize=(12, 12))
sns.heatmap(desc, annot=True, cmap='CMRmap_r', fmt='0.2f',
            ax=ax, linewidths=5, cbar=False,
            annot_kws={'size': 16})
plt.show()


# In[17]:


corr = data.corr()
mask = np.triu(np.ones_like(corr, dtype=bool))
plt.figure(figsize=(12, 12))
f, ax = plt.subplots(figsize=(12, 12))
sns.heatmap(corr, annot=True, cmap='CMRmap_r', mask=mask, fmt='0.2f', ax=ax, annot_kws={'size': 8})
plt.show()


# In[18]:


df = data[0:2000]


# In[19]:


df['instock_date'] = pd.to_datetime(df['instock_date'])


# In[20]:


plt.scatter(x='instock_date', y='price', data=df)


# In[21]:


data.columns


# ## Splitting of testing & training data

# In[22]:


X = data.drop(['gift_id' ,'price', 'instock_date', 'stock_update_date', 'uk_date1', 'uk_date2'], axis=1)
y = data['price'].values


# In[23]:


feature_list = list(X.columns)


# In[24]:


scaler = StandardScaler()
X = scaler.fit_transform(X)


# In[25]:


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=42)


# In[26]:


print('Training Features Shape:', X_train.shape)
print('Training Labels Shape:', y_train.shape)
print('Testing Features Shape:', X_test.shape)
print('Testing Labels Shape:', y_test.shape)


# ## Creating & Training Model

# ### Linear Regression

# In[27]:


lr = LinearRegression().fit(X_train, y_train)


# In[28]:


lr.score(X_test, y_test)


# ### RandomForest Regressor

# In[29]:


rf = RandomForestRegressor(random_state=42).fit(X_train, y_train)


# In[30]:


rf.score(X_test, y_test)


# In[31]:


y_pred = rf.predict(X_test)


# In[32]:


mse = mean_squared_error(y_test, y_pred)
mae = mean_absolute_error(y_test, y_pred)
rmse = np.sqrt(mse)
r2 = r2_score(y_test, y_pred)
print('Mean squared error:', mse)
print('Mean absolute error:', mae)
print('Root mean squared error:', rmse)
print('R2 Score:', r2)


# In[33]:


importances = list(rf.feature_importances_)


# In[34]:


feature_importances = [(feature, round(importance, 2)) for feature, importance in zip(feature_list, importances)]
feature_importances = sorted(feature_importances, key = lambda x: x[1], reverse = True)
[print('Variable: {:20} Importance: {}'.format(*pair)) for pair in feature_importances];


# In[35]:


x_values = list(range(len(importances)))
plt.figure(figsize=(10, 8))
plt.style.use('fivethirtyeight')
plt.bar(x_values, importances)
plt.xticks(x_values, feature_list, rotation='vertical')
# Axis labels and title
plt.ylabel('Importance')
plt.xlabel('Variable')
plt.title('Variable Importances')
plt.show()


# In[36]:


X2 = sm.add_constant(X)
est = sm.OLS(y, X2).fit()
print(est.summary())

